# Spike Report

## GRAPHICS PROGRAMMING - Particle Systems

### Introduction

In this spike we will come to grips with the very basics of particles, as making particle systems is part of the role of a technical artist.

### Goals

- For each of the following non-technical particle briefs/requests, come up with a short plan, and then build the particle system.
    - Rocket Launcher exhaust (the particle that comes out of a rocket launcher as you fire it)
    - A snow or rain particle system which can be attached to the player (camera) to simulate weather effects.
    - A red-to-yellow coloured mining laser beam, with “charging up” spikes at the start of the beam.
    - A world war 1 fragmentation grenade effect using the Mesh type.
- Use each of the following particle modules at least once. You can either include them in the above particles, or create additional particles to fulfil these requirements:
    - Colour Modules
    - Size modules
    - Spawn Modules
    - Velocity Modules
    - Acceleration Modules
    - Collision Modules

### Personnel

* Primary - Mitchell Bailey
* Secondary - Bradley Eales

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [Tutorial - Rain Particle System](https://nowyoshi.com/2016/10/09/tutorial-rain-particle-system-unreal-engine-4/)
* [Smoke Particle System](https://vimeo.com/149162343)
* [Grenade Mesh](https://www.turbosquid.com/3d-models/mk2-grenade-fbx-free/1042250)
* [Combining Static Meshes](https://answers.unrealengine.com/questions/123763/combining-static-mesh-in-editor.html)

### Tasks Undertaken

#### Rocket Launcher Exhaust

1. Created a material with a smoke texture, particle color and a lerp and set the material to a mask blend.
1. Created a new particle system called P_RocketLauncherExhaust with two particle emitters that use the new material.
1. Added initial color to the first emitter and changed the colours to red and black.
1. Adjusted the spawn times to fit the effect thats needed.
1. Adjusted the lifetime to the necessary values for the correct effect.
1. Allowed both emitters to be destroyed/killed after completed.

#### Rainfall

1. Followed instructions in Rain Particle System Tutorial linked above (instructions listed below).
    1. Make a new folder and call it M_rainfall
    1. Open the material and choose translucent in blend mode and make it two sided.
    1. Add a particle color and a radial gradient.
    1. Add two multiply nodes by holding M and left clicking twice.
    1. Connect the particle colour to A on the first multiply and connect the particle color alpha to A on the other multiply. Connect Radial gradient to both B’s on the multiply.
    1. Connect multiply 1 to Emissive color and multiply 2 to opacity.
    1. Hold 3 and left click, and choose a white close to light blue color.
    1. Save and close.
    1. Make a new particle system called P_rainfall.
    1. Click on required and find M_rainfall.
    1. Change type data to GPU_sprites.
    1. Change the spawn to 5000.
    1. Change lifetime to 5.
    1. Initial size Max = X = 3, Y = 3, Z = 10 —– Min = X = 3 Y = 3 Z = -10
    1. In initial velocity set all to 0 except Min Z to -500.
    1. Delete color over life.
    1. Add initial color and choose a white close to light blue.
    1. Add initial location (Seed) and choose = Max X = 1000 Y = 1000 Z = 1000 and Min Z = -200
    1. Add size by speed and choose the following. Speed scale = X = 0, Y = 5 —– Max scale = X = 0.4, Y =5.
    1. Add const Acceleration and set Z to -900.
    1. Add collision and set to 0.25 and change response to kill.
    1. Set bounds.
    1. Save and close and drag out your rain.

#### Beam

1. Create a new material with a TextureSample and a ParticleColor node attached to the base color.
    - A plain white image will work for the texture sample.
1. Create a new particle system called P_Beam.
    - Set the velocity to go straight up.
    - Set the initial color to red.
    - Adjusted the spawn constant to 1000.
    - Added the color over time and go from red to yellow.
        - Created 4 different points in the array that go from 0, 0.5 , 0.75, and 1 and adjusted colours accordingly (red, red, orange, yellow).

#### Grenade

1. Downloaded the free 3D mesh from resources used and merged the individual pieces into one actor.
    - Editor preferences -> search for merge actor and set up keybind.
    - Arranged grenade meshes inside the scene in the correct places.
    - Selected the grenade meshes inside the scene and merge them together.
1. Created a new particle system called P_GrenadeExplosion which will launch smaller versions of the grenade itself when the emitter is called.
    - Created an emitter that will show smoke (used the same setting from the rocket launcher).
    - Added another emitter that is set to launch the grenade mesh in every direction.
1. Created a new blueprint with the grenade mesh which delays the particle effect for a few seconds then spawns the emitters at the grenades location and rotation, and destroys the grenade actor.
    - Simulate phyics on the grenade actor.

### What we found out

- How to merge several meshes together.
- How particle systems can have different effects inside them.
- How to properly set up materials to be correctly utilised by particle systems.

### Open Issues/Risks

- The beam could potentially cause performance issues if emitting a large amount.

### Recommendations

- Suitable textures for the particles, such as the grenade explosion, since the functionality is working.
